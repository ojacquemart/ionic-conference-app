declare var FIREBASE_API_KEY: string;
declare var FIREBASE_AUTH_DOMAIN: string;
declare var FIREBASE_DATABASE_URL: string;
declare var FIREBASE_STORAGE_BUCKET: string;
declare var FIREBASE_MESSAGING_SENDER_ID: string;
