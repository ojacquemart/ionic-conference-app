import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, ToastController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';
import { SignupInfo } from '../../providers/signup-info.model';

import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-user',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: SignupInfo = {};
  submitted = false;

  constructor(private toastCtrl: ToastController,
              public navCtrl: NavController,
              public userData: UserData) {
  }

  onSignup(form: NgForm) {
    this.submitted = true;

    if (!form.valid) {
      return;
    }

    this.userData.signup(this.signup)
      .then(() => this.navCtrl.push(TabsPage))
      .catch((error: Error) => {
        let toast = this.toastCtrl.create({
          message: error.message
        });
        toast.present();
      });
  }
}
