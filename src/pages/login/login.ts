import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, ToastController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';
import { SignupInfo } from '../../providers/signup-info.model';


@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: SignupInfo = {};
  submitted = false;

  constructor(private toastCtrl: ToastController,
              public navCtrl: NavController, public userData: UserData) {
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (!form.valid) {
      return;
    }

    this.userData.login(this.login)
      .then(() => {
        this.navCtrl.push(TabsPage);
      })
      .catch((error: Error) => {
        let toast = this.toastCtrl.create({
          message: error.message,
        });
        toast.present();
      });
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }
}
