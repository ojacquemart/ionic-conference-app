import { Component } from '@angular/core';

import { AlertController, NavController, ToastController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  username: string;

  constructor(private toastCtrl: ToastController,
              public alertCtrl: AlertController,
              public nav: NavController,
              public userData: UserData) {

  }

  ngAfterViewInit() {
    this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  changeUsername() {
    let alert = this.alertCtrl.create({
      title: 'Change Username',
      buttons: [
        'Cancel'
      ]
    });
    alert.addInput({
      name: 'username',
      value: this.username,
      placeholder: 'username'
    });
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        this.userData.setUsername(data.username)
          .then(() => this.username = data.username)
          .catch((error: Error) => {
            const toast = this.toastCtrl.create({
              message: error.message,
            });
            toast.present();
          });
      }
    });

    alert.present();
  }

  getUsername() {
    this.userData.getUser$()
      .map(user => user.displayName)
      .subscribe(username => {
        this.username = username;
      });
  }

  changePassword() {
    console.log('Clicked to change password');
  }

  logout() {
    this.userData.logout();
    this.nav.setRoot('LoginPage');
  }

  support() {
    this.nav.push('SupportPage');
  }
}
