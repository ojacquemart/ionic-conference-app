export interface SignupInfo {
  email?: string;
  password?: string;
}
