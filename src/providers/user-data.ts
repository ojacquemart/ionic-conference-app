import { Injectable } from '@angular/core';

import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { SignupInfo } from './signup-info.model';
import { UserInfo } from 'firebase/app';

@Injectable()
export class UserData {
  _favorites: string[] = [];
  HAS_LOGGED_IN = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';

  constructor(
    private auth: AngularFireAuth,
    private db: AngularFireDatabase,
    public events: Events,
    public storage: Storage
  ) {}

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  };

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  };

  removeFavorite(sessionName: string): void {
    let index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  };

  login(signupInfo: SignupInfo): firebase.Promise<void> {
    return this.auth.auth.signInWithEmailAndPassword(signupInfo.email, signupInfo.password)
      .then(() => {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(signupInfo.email);
        this.events.publish('user:login');
      });
  };

  signup(signupInfo: SignupInfo): firebase.Promise<void> {
    return this.auth.auth.createUserWithEmailAndPassword(signupInfo.email, signupInfo.password)
      .then((user: UserInfo) => this.saveUser(user))
      .then((user: UserInfo) => {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(signupInfo.email);

        this.events.publish('user:signup');

        return user;
      });
  };

  private saveUser(user: UserInfo): firebase.Promise<void> {
    console.log('userData @ save user', user);

    return this.getUsersCollection(user)
      .set({
        uid: user.uid,
        displayName: user.email
      })
  }

  logout(): firebase.Promise<void> {
    return this.auth.auth.signOut()
      .then(() => {
        this.storage.remove(this.HAS_LOGGED_IN);
        this.storage.remove('username');
        this.events.publish('user:logout');
      })
      .catch(error => console.error('userData @ logout', error));
  };

  setUsername(username: string): firebase.Promise<void> {
    return this.getUsersCollection(this.auth.auth.currentUser)
      .update({
        displayName: username
      })
      .then(() => this.storage.set('username', username));
  };

  getUser$(): Observable<UserInfo> {
    return this.getUsersCollection(this.auth.auth.currentUser);
  }

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  };

  checkHasSeenTutorial(): Promise<string> {
    return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
      return value;
    });
  };

  private getUsersCollection(user: UserInfo) {
    return this.db.object(`/users/${user.uid}`);
  }

}
