require('dotenv').config();

var path = require('path');
var config = require('@ionic/app-scripts/config/webpack.config.js');
var logger = require('@ionic/app-scripts/dist/logger/logger.js');

var webpack = require('webpack');

logger.Logger.info('config started');

config.resolve.alias = {
  "@app/config": path.resolve('./src/config/config.' + process.env.IONIC_ENV + '.ts')
};

config.plugins = config.plugins || [];
config.plugins.push(
  new webpack.DefinePlugin({
    FIREBASE_API_KEY: getEnvValue('FIREBASE_API_KEY'),
    FIREBASE_AUTH_DOMAIN: getEnvValue('FIREBASE_AUTH_DOMAIN'),
    FIREBASE_DATABASE_URL: getEnvValue('FIREBASE_DATABASE_URL'),
    FIREBASE_STORAGE_BUCKET: getEnvValue('FIREBASE_STORAGE_BUCKET'),
    FIREBASE_MESSAGING_SENDER_ID: getEnvValue('FIREBASE_MESSAGING_SENDER_ID')
  })
);
if (process.env.IONIC_ENV === 'prod') {
  // This helps ensure the builds are consistent if source hasn't changed.
  config.plugins.push(new webpack.optimize.OccurrenceOrderPlugin());
}

function getEnvValue(envKey) {
  const ionicEnv = (process.env.IONIC_ENV || '').toUpperCase();
  const fullEnvKey = ionicEnv + '_' + envKey;

  const envValue = process.env[fullEnvKey];

  if (!envValue) {
    logger.Logger.warn(`Env value '${fullEnvKey}' is not set`);

    return null;
  }

  return JSON.stringify(envValue);
}

module.exports = config;
