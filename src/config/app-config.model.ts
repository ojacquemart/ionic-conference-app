import { FirebaseAppConfig } from 'angularfire2';

// this is the model class used in customTypings#appConfig.
export interface AppConfig {
  production?: boolean;
  firebase: FirebaseAppConfig;
}

export const APP_CONFIG: AppConfig = {
  firebase: {
    apiKey: FIREBASE_API_KEY,
    authDomain: FIREBASE_AUTH_DOMAIN,
    databaseURL: FIREBASE_DATABASE_URL,
    storageBucket: FIREBASE_STORAGE_BUCKET,
    messagingSenderId: FIREBASE_MESSAGING_SENDER_ID
  }
};
